package com.cibersys.api.rest.auth.controller;

import com.cibersys.api.rest.auth.model.Application;
import com.cibersys.api.rest.auth.model.AuthApplication;
import com.cibersys.api.rest.auth.model.OwnClient;
import com.cibersys.api.rest.auth.response.Response;
import com.cibersys.api.rest.auth.security.KeyHandler;
import com.cibersys.api.rest.auth.service.ApplicationService;
import com.cibersys.api.rest.auth.service.AuthApplicationService;
import com.cibersys.api.rest.auth.service.OwnClientService;
import com.cibersys.api.rest.auth.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.UUID;


/**
 * <p>
 * Clase que define los m&eacute;todos de controlador para el manejo de informaci&oacute;n
 * relacionada con una tarjeta de presentacion.
 * </p>
 *
 * @author <a href="mailto:jose.gomez@cibersys.com">Jos&eacute; Gregorio G&oacute;mez</a>
 * @version 1.0
 * 
 */
@Controller
@RequestMapping(value = "/cibersys-api-rest/v1.0.0")
//desarrollo.cibersys.com/context/api-rest/v1.0.0/
public class LoginController {

    private static Log log = LogFactory.getLog(LoginController.class.getName());

    @Autowired
    OwnClientService ownClientService;
    @Autowired
    ApplicationService applicationService;
    @Autowired
    AuthApplicationService authApplicationService;
    private KeyHandler keyHandler;


    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<? extends Response> home(){
        Response response = new Response();
        response.setCode("200");
        response.setMessage("Hola");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    /*
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes="application/json",
			produces = "application/json")
	public ResponseEntity<?> doLogin(@RequestBody OwnClient user) {
        Response response = new Response(); //General response
		if (user == null){ //Verify if user is null
            response.setCode("301");
            response.setMessage("RequestBody is empty");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        if (user.getClientId() != null ){
            // ClientId cant be null
            /*if(tokenHandler.isValidToken(user.getClientId())){
                // It has to be valid token
                if (user.getEmail() != null && user.getPassword() != null) {
                    /*BodyRequest must have a password
                     * And an email
                     */ /*
                    OwnClient userFound = ownClientService.findByEmail(user.getEmail());
                    if (userFound == null){
                        // User name not found on db
                        response.setCode("303");
                        response.setMessage("Username is not valid");
                        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                    }
                    if (userFound.getPassword() != user.getPassword()){
                        //User found by Login but password is not correct
                        response.setCode("304");
                        response.setMessage("Incorrect password");
                        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                    }
                    /* I have no more validations
                     * Token and user are valid
                     * Proceed.
                     */ /*
                    response.setCode("200");
                    response.setMessage("User logged in");
                    return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
                }
            }else{ // If not valid
                response.setCode("302");
                response.setMessage("Token cant be null and must be valid");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }
        // Token was null
        response.setCode("305");
        response.setMessage("Token is a mandatory field");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}//End controller /login
    */

    private int signupValidations (OwnClient ownClient){

        //Empty request
        if (ownClient == null) return 1;

        //Empty username
        if (ownClient.getFirstname() == null && ownClient.getFirstname().isEmpty()) return 2;

        //Empty lastname
        if (ownClient.getLastname() == null && ownClient.getLastname().isEmpty()) return 3;

        //Empty password
        if (ownClient.getPassword() == null && ownClient.getPassword().isEmpty() ) return 4;

        //Empty email
        if (ownClient.getEmail() == null && ownClient.getEmail().isEmpty()) return 5;

        //User exists
        OwnClient userFound = ownClientService.findByEmail(ownClient.getEmail());
        if (userFound != null) return 6;

        //Success
        return 0;
    }

    private HashMap<String,String> handleKeys(OwnClient ownClient){
        keyHandler = new KeyHandler();
        String ApiKey = keyHandler.generateApiKey(ownClient);
        String clientId = UUID.randomUUID().toString(); //Generating a Java UUID
        OwnClient clientidExist = ownClientService.findByClientId(clientId); // Verify if it exists
        if (clientidExist != null) { // It does exists already ... Bad luck
            clientId = UUID.randomUUID().toString(); // Generating a new UUID
            clientidExist = ownClientService.findByClientId(clientId); // Verifying again
            if (clientidExist != null) { // It does exists already ... WEIRD !!!!
                if (clientidExist.getEmail() != null)
                    /*
                        Checking if the user has an email,
                        to contact the client.
                     */
                    return null;
            }
        }
        HashMap<String,String> keys = new HashMap<>();
        keys.put("apikey", ApiKey);
        keys.put("clientId", clientId);
        return keys;
    }

	@RequestMapping(value = "/signup", method = RequestMethod.POST, consumes = "application/json",
			produces = "application/json")
	public ResponseEntity<?> doSignup(@RequestBody OwnClient ownClient) throws UnsupportedEncodingException {
        Response response = new Response(); // Object to handle every single respone.
        /*
            Im about to validate the request body and its fields
         */
        switch (signupValidations(ownClient)){
            case 1:
                response.setCode("301");
                response.setMessage("Empty request body");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            case 2:
                response.setCode("302");
                response.setMessage("Empty first name");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            case 3:
                response.setCode("303");
                response.setMessage("Empty last name");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            case 4:
                response.setCode("304");
                response.setMessage("Empty password");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            case 5:
                response.setCode("305");
                response.setMessage("Empty email");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            case 6:
                response.setCode("306");
                response.setMessage("User already exists");
                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
        }

        /*
         *  All the necessary fields are valid,
         *  and user does not exist yet.
         *  Lets generate a token.
         */
        HashMap<String, String> keys = handleKeys(ownClient);
        if (keys == null){
            log.fatal("Por favor revisar " + ownClient.getEmail());
            response.setCode("510");
            response.setMessage("Seguridad.");
            return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
        }
        /*
            I will store the ApiKey and clientId inside a hashMap
            If the return was null it means, that 2 UUID's were
            generated and both already exists. Contact the email
            of the user generating the request.
            "apikey" "clientId"
         */

        /*
            UUID was sucessfully generated, lets validate tho
            Im gonna store the user and his new app
         */
        if (keys.get("clientId") != null && !keys.get("clientId").isEmpty()){
            // Add primary key ...
            ownClient.setClientId(keys.get("clientId"));
            // Before saving the ownClient
            OwnClient saved = ownClientService.save(ownClient);
            // Check if ownClient is saved correctly
            if (saved != null) {
                /*
                    User was successfully added
                    Lets create an app
                 */
                Application application = new Application();
                application.setName("default");
                application.setApiKey(keys.get("apikey"));
                application.setClientId(saved);
                application.setDate(new DateUtil().getDate());
                // Before saving the app
                Application app_saved = applicationService.save(application);
                // Check if app was saved correctly
                if (app_saved != null){
                    /*
                        Application was successfully saved
                        Lets create auth token and store it
                      */
                    AuthApplication authApplication = new AuthApplication();
                    String tokenAuth = keyHandler.generateTokenAuth(app_saved, keys.get("apikey"));
                    authApplication.setTokenAuth(tokenAuth);
                    authApplication.setApplication(app_saved);
                    authApplication.setDate(new DateUtil().getDate());
                    // Before saving the AuthToken
                    AuthApplication auth_saved = authApplicationService.save(authApplication);
                    // Check if auth token was saved correctly
                    if (auth_saved != null){
                        // Success - everything went smooth
                        response.setCode("200");
                        response.setMessage("Sign up - success");
                        return new ResponseEntity<>(response, HttpStatus.OK);
                    }else{
                        // Rolling back ... Just in case
                        applicationService.delete(app_saved);
                        ownClientService.delete(saved);
                        response.setCode("307");
                        response.setMessage("User was not added, try again");
                        return new ResponseEntity<>(response, HttpStatus.OK);
                    }
                }else{
                    /*
                       App was not stored correctly, lets roll back
                       Just in case...
                     */
                    ownClientService.delete(saved);
                    response.setCode("307");
                    response.setMessage("User was not added, try again");
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
            }else{
                // User was not added
                response.setCode("307");
                response.setMessage("User was not added, try again");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }else{
            // Token was not created successfully
            response.setCode("308");
            response.setMessage("An error has occurred");
            return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
        }
	} //End controller /signup
}// End class


