package com.cibersys.api.rest.auth.repository;

import com.cibersys.api.rest.auth.model.AuthApplication;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
public interface AuthApplicationRepository extends CrudRepository<AuthApplication, String > {
}
