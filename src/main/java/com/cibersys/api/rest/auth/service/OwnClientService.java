package com.cibersys.api.rest.auth.service;

import com.cibersys.api.rest.auth.model.OwnClient;
import com.cibersys.api.rest.auth.repository.OwnClientRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Royner Mejia on 4/8/2016.
 */
@Service
@Transactional
public class OwnClientService {

    private static Log log = LogFactory.getLog(OwnClientService.class.getName());

    @Autowired
    private OwnClientRepository ownClientRepository;

    public OwnClient findByEmailAndCompany(String email, String company){
        return ownClientRepository.findByEmailAndCompany(email,company);
    }

    public OwnClient save(OwnClient user){
        return ownClientRepository.save(user);
    }

    public void delete(OwnClient saved) { ownClientRepository.delete(saved);
    }

    public OwnClient findByEmail(String email) {
        return ownClientRepository.findByEmail(email);
    }

    public OwnClient findByClientId(String clientId) { return ownClientRepository.findByClientId(clientId); }
}
