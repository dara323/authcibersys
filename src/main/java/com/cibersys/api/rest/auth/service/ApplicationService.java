package com.cibersys.api.rest.auth.service;

import com.cibersys.api.rest.auth.model.Application;
import com.cibersys.api.rest.auth.repository.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
@Service
@Transactional
public class ApplicationService {

    @Autowired
    ApplicationRepository applicationRepository;

    public Application save(Application application){
        return applicationRepository.save(application);
    }

    public void delete(Application app_saved) {
        applicationRepository.delete(app_saved);
    }
}
