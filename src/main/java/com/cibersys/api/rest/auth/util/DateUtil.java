package com.cibersys.api.rest.auth.util;

import java.sql.Date;
import java.text.ParseException;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
public class DateUtil {

    private Date date;

    public DateUtil() {
        java.util.Date dt = new java.util.Date();

        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String currentTime = sdf.format(dt);
        try {
            Date date = new Date(sdf.parse(currentTime).getTime());
            this.date = date;
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }
    }

    public DateUtil(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
