package com.cibersys.api.rest.auth.repository;

import com.cibersys.api.rest.auth.model.Application;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
public interface ApplicationRepository extends CrudRepository<Application, String> {
}
