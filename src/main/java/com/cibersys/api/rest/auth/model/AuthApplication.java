package com.cibersys.api.rest.auth.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
@Entity
@Table(name = "auth_app")
public class AuthApplication implements Serializable {

    @Id
    @Column(name = "token_auth")
    private String tokenAuth;

    @ManyToOne
    @JoinColumn(name = "api_key")
    private Application application;

    @Column(name = "creation_date")
    @NotNull
    private Date date;

    public AuthApplication() {
    }

    public AuthApplication(String tokenAuth, Application application, Date date) {
        this.tokenAuth = tokenAuth;
        this.application = application;
        this.date = date;
    }

    public String getTokenAuth() {
        return tokenAuth;
    }

    public void setTokenAuth(String tokenAuth) {
        this.tokenAuth = tokenAuth;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application apikey) {
        this.application = apikey;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
