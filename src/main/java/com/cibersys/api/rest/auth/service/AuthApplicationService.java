package com.cibersys.api.rest.auth.service;

import com.cibersys.api.rest.auth.model.AuthApplication;
import com.cibersys.api.rest.auth.repository.AuthApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
@Service
@Transactional
public class AuthApplicationService {

    @Autowired
    AuthApplicationRepository authApplicationRepository;

    public AuthApplication save(AuthApplication authApplication) {
        return authApplicationRepository.save(authApplication);
    }
}
