package com.cibersys.api.rest.auth.response;

import java.io.Serializable;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
public class SignupResponse implements Serializable{

    private String apikey;
    private String clientid;
    private String signature;

    public SignupResponse() {
    }

    public SignupResponse(String apikey, String clientid, String signature) {
        this.apikey = apikey;
        this.clientid = clientid;
        this.signature = signature;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
