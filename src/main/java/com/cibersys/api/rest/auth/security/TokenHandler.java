package com.cibersys.api.rest.auth.security;

import com.cibersys.api.rest.auth.model.Application;
import com.cibersys.api.rest.auth.model.OwnClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.HashMap;

/**
 * Created by Royner Mejia on 1/8/2016.
 */
public class TokenHandler {

    private static final String SEPARATOR_SPLITTER = "\\.";

    public TokenHandler() {
    }

    public String generateString(int len){
        String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder( len );
        SecureRandom rnd = new SecureRandom();

        for( int i = 0; i < len; i++ )
            sb.append( alphabet.charAt( rnd.nextInt(alphabet.length()) ) );
        return sb.toString();
    }

    public HashMap<String,String> createTokenForUser(OwnClient user, String apikey) {
        String signature = generateString(10);
        byte[] key = new byte[64];
        new SecureRandom().nextBytes(key);
        try {
            String token = Jwts.builder()
                    .setHeaderParam("APIKEY",apikey)
                    .setSubject(toJSON(user).toString())
                    .signWith(SignatureAlgorithm.HS512, signature.getBytes("UTF-8"))
                    .compact();
            HashMap<String, String> retorno = new HashMap<>();
            retorno.put("token", token);
            retorno.put("signature", signature);
            return retorno;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String createTokenForApp(Application app_saved, String apikey) {
        String signature = generateString(10);
        byte[] key = new byte[64];
        new SecureRandom().nextBytes(key);
        try {
            String token = Jwts.builder()
                    .setHeaderParam("APIKEY",apikey)
                    .setSubject(toJSON(app_saved).toString())
                    .signWith(SignatureAlgorithm.HS512, signature.getBytes("UTF-8"))
                    .compact();
            return token;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public OwnClient fromJSON(final byte[] userBytes) {
        try {
            return new ObjectMapper().readValue(new ByteArrayInputStream(userBytes), OwnClient.class);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public byte[] toJSON(OwnClient user) {
        try {
            return new ObjectMapper().writeValueAsBytes(user);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    public Application appFromJSON(final byte[] appBytes) {
        try {
            return new ObjectMapper().readValue(new ByteArrayInputStream(appBytes), Application.class);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public byte[] toJSON(Application application) {
        try {
            return new ObjectMapper().writeValueAsBytes(application);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }


}
