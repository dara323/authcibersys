package com.cibersys.api.rest.auth.model;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
@Entity
@Table(name = "application")
public class Application implements Serializable {

    @Id
    @Column(name = "api_key")
    private String apiKey;


    @Column(name = "name")
    @NotNull
    private String name;

    @ManyToOne
    @JoinColumn(name = "client_id")
    @JsonIgnore
    private OwnClient clientId;

    @Column(name = "creation_date")
    @NotNull
    private Date date;

    public Application() {
    }

    public Application(String apiKey, String name, OwnClient clientId, Date date) {
        this.apiKey = apiKey;
        this.name = name;
        this.clientId = clientId;
        this.date = date;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OwnClient getClientId() {
        return clientId;
    }

    public void setClientId(OwnClient clientId) {
        this.clientId = clientId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
