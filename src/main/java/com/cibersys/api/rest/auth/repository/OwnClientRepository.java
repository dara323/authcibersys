package com.cibersys.api.rest.auth.repository;

import com.cibersys.api.rest.auth.model.OwnClient;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Royner Mejia on 4/8/2016.
 */
public interface OwnClientRepository extends CrudRepository<OwnClient, String> {

    OwnClient findByClientId(String clientId);

    OwnClient findByEmailAndCompany(String email, String company);

    OwnClient findByEmail(String email);
}
