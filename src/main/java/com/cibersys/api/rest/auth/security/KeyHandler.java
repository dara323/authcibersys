package com.cibersys.api.rest.auth.security;

import com.cibersys.api.rest.auth.model.Application;
import com.cibersys.api.rest.auth.model.OwnClient;

import java.util.HashMap;

/**
 * Created by Royner Mejia on 5/8/2016.
 */
public class KeyHandler {

    private TokenHandler tokenHandler;

    public KeyHandler() {
        this.tokenHandler = new TokenHandler();
    }

    public HashMap<String, String> generateClientId(OwnClient user, String apikey) {
        return tokenHandler.createTokenForUser(user, apikey);
    }

    public String generateApiKey(OwnClient user) {
        return tokenHandler.generateString(40);

    }

    public String generateTokenAuth(Application app_saved, String apikey) {
        return tokenHandler.createTokenForApp(app_saved, apikey);
    }
}
